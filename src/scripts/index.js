const navBtn = document.querySelector(".nav__button");
const navBtnOpened = document.querySelector(".nav__icon--opened");
const navBtnClosed = document.querySelector(".nav__icon--closed");
const menu = document.querySelector(".nav__content-wrapper");

navBtn.addEventListener("click", btnClickingHandler);
function btnClickingHandler() {
  menu.classList.toggle("nav__content-wrapper--open");
  navBtnClosed.classList.toggle("none");
  navBtnOpened.classList.toggle("none");
}
